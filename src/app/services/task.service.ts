import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  domain: String = 'http://localhost:3000';

  constructor(private http: HttpClient) {}

  getTasks() {
    return this.http.get(`${this.domain}/users`).pipe(map(res => res));
  }
  addTask(newTask) {
    return this.http
      .post(`${this.domain}/users`, newTask)
      .pipe(map(res => res));
  }
  deleteTask(id) {
    return this.http.delete(`${this.domain}/users/${id}`).pipe(map(res => res));
  }
  updateTask(newTask) {
    return this.http
      .put(`${this.domain}/users/${newTask.id}`, newTask)
      .pipe(map(res => res));
  }
}
