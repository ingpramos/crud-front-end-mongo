import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: []
})
export class DataComponent implements OnInit {
  tasks: any = [];
  task: any = {};
  info: any;

  constructor(private _taskService: TaskService) {
    this._taskService.getTasks().subscribe(tasks => {
      this.tasks = tasks;
      console.log(tasks);
    });
  }

  ngOnInit() {}

  // datos = [
  //   {
  //     nombre: 'pablo',
  //     apellido: 'ramos',
  //     email: 'pramos@mail.com'
  //   },
  //   {
  //     nombre: 'charld',
  //     apellido: 'xavier',
  //     email: 'cxavier@mail.com'
  //   },
  //   {
  //     nombre: 'trush',
  //     apellido: 'async',
  //     email: 'tasync@mail.com'
  //   }
  // ];

  addTask(data: NgForm) {
    // console.log('NgForm', formulario);
    console.log('valor', data.value);

    const newTask = {
      task: this.info
    };
    this._taskService.addTask(newTask).subscribe(task => {
      this.tasks.push(task);
      console.log(task);
      this.task = '';
    });
  }

  deleteTask(id) {
    const tasks = this.tasks;
    this._taskService.deleteTask(id).subscribe(data => {
      for (let i = 0; i < tasks.length; i++) {
        if (tasks[i]._id == id) {
          tasks.splice(i, 1);
        }
      }
      console.log(data);
    });
  }

  editTask(_id, form) {
    const newTask = {
      id: _id,
      task: form.value.edit
    };
    console.log(form.value.edit);
    this._taskService.updateTask(newTask).subscribe(res => {
      console.log(res);
    });
  }
}
